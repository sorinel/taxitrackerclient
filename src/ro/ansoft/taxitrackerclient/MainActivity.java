package ro.ansoft.taxitrackerclient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends Activity {

	private GoogleMap googleMap;
	private Timer timerGetCoordinates;
	private JSONParser jsonParser;
	private Marker marker;
	private double mLatitude = -1;
	private double mLongitude = -1;
	private static final int TIMER_PERIOD = 5000;
	private static final String urlGetCoordinates = "http://ansoft.ro/taxi/get_coordinates.php";
	private static final String TAG = "TaxiTrackerClient";
	private static final String LAT = "lat";
	private static final String LONG = "long";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		jsonParser = new JSONParser();
		initializeMap();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onStart() {
	    super.onStart();
	    timerGetCoordinates = new Timer();
	    timerGetCoordinates.schedule(new TimerTask() {
			@Override
			public void run() {
				getNewCoordinates();
			}
		}, new Date(), TIMER_PERIOD);
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    initializeMap();
	}
	
	@Override
	protected void onStop() {
	    super.onStop();
	    timerGetCoordinates.cancel();
	    timerGetCoordinates = null;
	}

	@Override
	protected void onDestroy() {
	    super.onDestroy();
	}

	private void initializeMap() {
		if(googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

			if(googleMap == null) {
				Toast.makeText(this, "Unable to create map", Toast.LENGTH_LONG).show();
			}
		}

		//galati
		mLatitude = 45.44;
		mLongitude = 28.04;
		showMarker();
	}

	private void showMarker() {
		if(!isOnline()) {
			Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show();
			return;
		}
		googleMap.clear();
		marker = googleMap.addMarker(new MarkerOptions()
		.position(new LatLng(mLatitude, mLongitude))
		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
		.title("Taxi position"));

		CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(15).build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void getNewCoordinates() {
		if(!isOnline()) {
			MainActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(MainActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
				}
			});
			return;
		}
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		JSONObject json = jsonParser.makeHttpRequest(urlGetCoordinates, JSONParser.POST, params);
		try {
			Log.d(TAG, "" + json.getDouble(LAT) + " " + json.getDouble(LONG));
			mLatitude = json.getDouble(LAT);
			mLongitude = json.getDouble(LONG);
			MainActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(MainActivity.this, "new position: " + mLatitude + " " + mLongitude, Toast.LENGTH_SHORT).show();
					showMarker();
				}
			});
		} catch (JSONException e) {
			Log.e(TAG, "JSON Error", e);
		}
	}

	private boolean isOnline() {
		ConnectivityManager connec = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connec != null && (connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) || 
		    (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED)) { 
		        return true;
		} else if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
		         connec.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED ) {            
				return false;
		}
		return false;
	}
	
}
